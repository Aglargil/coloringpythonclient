from cProfile import label
import matplotlib.pyplot as plt
from matplotlib.patches import RegularPolygon
import numpy as np



coord = [[0,0,0],[0,1,-1],[-1,1,0],[-1,0,1],[0,-1,1],[1,-1,0],[1,0,-1]]
colors = [["White"],["White"],["White"],["White"],["White"],["White"],["White"]]
    
def draw_board(coord, colors):
    # Horizontal cartesian coords
    hcoord = [c[0] for c in coord]

    # Vertical cartersian coords
    vcoord = [2. * np.sin(np.radians(60)) * (c[1] - c[2]) /3. for c in coord]

    for i in range(len(vcoord)):
        temp = vcoord[i]
        vcoord[i] = -hcoord[i]
        hcoord[i] = temp

    fig, ax = plt.subplots(1, figsize=(10, 10))
    ax.set_aspect('equal')

    # Add some coloured hexagons
    for x, y, c, l in zip(hcoord, vcoord, colors, coord):
        color = c[0]
        hex = RegularPolygon((x, y), numVertices=6, radius=2. / 3, 
                            orientation=np.radians(120), facecolor = color,
                            alpha=0.3, edgecolor='k')
        ax.add_patch(hex)
        # Also add a text label
        ax.text(x, y+0.2, l[0:2], ha='center', va='center', size=6)

    # Also add scatter points in hexagon centres
    ax.scatter(hcoord, vcoord, alpha=0)

    plt.show()

if __name__ == "__main__":

    draw_board(coord, colors)
from __future__ import print_function

import logging
from re import L
from turtle import position

import grpc
import helloworld_pb2
import helloworld_pb2_grpc
import time

from hex import draw_board

def plot(response):
    coord = []
    colors = []

    for grid in response.grid_list:
        cube = grid.position
        coord.append([cube.q, cube.r, cube.s])
        if grid.item_list:
            colors.append("c")
        else:
            colors.append(grid.color)
            
    draw_board(coord, colors)

def plot_map():
    with grpc.insecure_channel('175.178.172.98:50051') as channel:
        stub = helloworld_pb2_grpc.MaterialStub(channel)
        response = stub.GetMap(helloworld_pb2.MapType(type=1))
    plot(response)
    return response

def login(account):
    with grpc.insecure_channel('175.178.172.98:50051') as channel:
        stub = helloworld_pb2_grpc.RoleStub(channel)
        response = stub.LoginRole(helloworld_pb2.RoleLoginInfo(account=account))
    print(response.token)
    return response.token
# with grpc.insecure_channel('175.178.172.98:50051') as channel:
#     stub = helloworld_pb2_grpc.RoleStub(channel)
#     response = stub.LoginRole(helloworld_pb2.RoleLoginInfo(account="54321"))
# while True:
#     with grpc.insecure_channel('175.178.172.98:50051') as channel:
#         stub = helloworld_pb2_grpc.ConnectionStub(channel)
#         response, call = stub.HeartBeat.with_call(
#             helloworld_pb2.HelloRequest(name="heartbeat"),
#             metadata=(("token",token),)
#             )
#     print(response)
#     time.sleep(3)
# with grpc.insecure_channel('175.178.172.98:50051') as channel:
#     stub = helloworld_pb2_grpc.MaterialStub(channel)
#     response = stub.GetGridByDirection(helloworld_pb2.GetGridByDirectionInfo(
#         position = {'q':1, 'r':2},
#         direction = 1,
#         ))
# print(response)
# a = plot_map()

# setmapcolor
# response.grid_list[42].color = "red"
# with grpc.insecure_channel('175.178.172.98:50051') as channel:
#     stub = helloworld_pb2_grpc.MaterialStub(channel)
#     response = stub.SetMapColor(response)
# plot_map()

def A(q, r, action_type, direction, token):
    with grpc.insecure_channel('175.178.172.98:50051') as channel:
        stub = helloworld_pb2_grpc.MaterialStub(channel)
        response = stub.DoBattle(
            helloworld_pb2.BattleInfo(
                action = {
                    'action_type':action_type,
                    'action_args':{'direction':direction}
                },
                position = {'q':q, 'r':r}             
            ),
            metadata=(
                ("token",token),
            )
        )
    a = plot_map()
 


# account1_token = login("123456")
# A(0,0,2,0, account1_token)
# A(2,3,2,5, account1_token)

# account2_token = login("54321")
# A(6,1,2,3, account2_token)


# RoomService
account1_token = login("123456")

account2_token = login("54321")

def RoomPlotMap(room_id):
    with grpc.insecure_channel('175.178.172.98:50051') as channel:
        stub = helloworld_pb2_grpc.RoomStub(channel)
        response = stub.GetMap(
            helloworld_pb2.MapType(type=1),
            metadata=(
                ("room_id",str(room_id)),
            )
        )
    plot(response)
    return response
def RoomAttack(q, r, action_type, direction, token, room_id):
    with grpc.insecure_channel('175.178.172.98:50051') as channel:
        stub = helloworld_pb2_grpc.RoomStub(channel)
        response = stub.DoBattle(
            helloworld_pb2.BattleInfo(
                action = {
                    'action_type':action_type,
                    'action_args':{'direction':direction}
                },
                position = {'q':q, 'r':r}             
            ),
            metadata=(
                ("token",token),
                ("room_id",str(room_id)),
            )
        )
    RoomPlotMap(room_id)
def CreateRoom(token):
    with grpc.insecure_channel('175.178.172.98:50051') as channel:
        stub = helloworld_pb2_grpc.RoomStub(channel)
        response = stub.CreateRoom(
                helloworld_pb2.CreateRoomInfo(
                    type = 1,
                    player_number = 2            
                ),
                metadata=(
                    ("token",token),
                )
            )
    return response.room_id
def EnterRoom(token, room_id):
    with grpc.insecure_channel('175.178.172.98:50051') as channel:
        stub = helloworld_pb2_grpc.RoomStub(channel)
        response = stub.EnterRoom(
                helloworld_pb2.EnterRoomInfo(
                    room_id = room_id         
                ),
                metadata=(
                    ("token",token),
                )
            )
    return response
def ListRoom():
    with grpc.insecure_channel('175.178.172.98:50051') as channel:
        stub = helloworld_pb2_grpc.RoomStub(channel)
        response = stub.ListRoom(
                helloworld_pb2.Empty(),
            )
    return response
room_id = CreateRoom(account1_token)
ListRoom()
EnterRoom(account2_token, room_id)

RoomAttack(0,0,2,0, account1_token, room_id)
RoomAttack(2,3,2,1, account1_token, room_id)
RoomAttack(3,-3,2,1, account2_token, room_id)